echo "Running on $([Environment]::MachineName)..."

#$Env:Path.Split(';')

& "cmake.exe" "-G" "MinGW Makefiles" "-B" "d:\hw_build"
if(!$?) { Exit $LASTEXITCODE }

& "cmake.exe" "--build" "d:\hw_build"
if(!$?) { Exit $LASTEXITCODE }

& "cmake.exe" "--install" "d:\hw_build" "--prefix" "d:\hw_install"
if(!$?) { Exit $LASTEXITCODE }


