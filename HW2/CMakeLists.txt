﻿# CMakeList.txt: CMake-Projekt für "HW2". Schließen Sie die Quelle ein, und definieren Sie
# projektspezifische Logik hier.
#
cmake_minimum_required (VERSION 3.8)

# Fügen Sie der ausführbaren Datei dieses Projekts eine Quelle hinzu.
add_executable (HW2 "HW2.cpp" "HW2.h")

install(TARGETS HW2 DESTINATION .) 

# TODO: Fügen Sie bei Bedarf Tests hinzu, und installieren Sie Ziele.
